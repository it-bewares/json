/**
 *
 * Copyright 2018 Markus Goellnitz.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.bewares.json.io.exception;

/**
 * Thrown to indicate that the application has attempted to generate a JSON
 * String from its tree representative, but that the tree is self-containing.
 * Hence a recursion accured which ultimatly would have led to a non-final
 * generation.
 *
 * JSON refers to the JavaScript Object Notation Data Interchange Format in its
 * definition by the IETF in RFC 8259.
 *
 * @author Markus Goellnitz
 * @see <a href="https://tools.ietf.org/html/rfc8259">RFC 8259</a>
 */
public class JSONRecursionException extends IllegalArgumentException {

    private static final long serialVersionUID = 790845189070085707L;

    /**
     * Allocates a {@code JSONRecursionException} with no detail message.
     */
    public JSONRecursionException() {
        super();
    }

    /**
     * Allocates a {@code JSONRecursionException} with the specified detail
     * message.
     *
     * @param message the detail message
     */
    public JSONRecursionException(String message) {
        super(message);
    }

    /**
     * Allocates a {@code JSONRecursionException} with the specified detail
     * message and cause.
     *
     * @param message the detail message
     * @param cause the cause for this exception
     */
    public JSONRecursionException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Allocates a {@code JSONRecursionException} with the specified cause.
     *
     * @param cause the cause for this exception
     */
    public JSONRecursionException(Throwable cause) {
        super(cause);
    }

}
