/**
 *
 * Copyright 2018 Markus Goellnitz.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.bewares.json.io.generator;

import java.io.IOException;
import java.io.Writer;
import it.bewares.json.io.exception.JSONFormatException;
import it.bewares.json.tree.JSONValue;
import java.io.StringWriter;

/**
 * All implementations of this Interface shall put out valid JSON. The purpose
 * of this interface is to give more flexibility and to let applications use or
 * implement their own Generator which may output JSON extensions.
 *
 * All implementations must either override the
 * {@link #write(it.bewares.json.tree.JSONValue, java.io.Writer) write} or the
 * {@link #generate(it.bewares.json.tree.JSONValue) generate} method due to the
 * infinte recursion that would occure otherwise.
 *
 * Implementations should use the {@code @Singleton} annotation to make a
 * dependency injection possible.
 *
 * JSON refers to the JavaScript Object Notation Data Interchange Format in its
 * definition by the IETF in RFC 8259.
 *
 * @author Markus Goellnitz
 * @see <a href="https://tools.ietf.org/html/rfc8259">RFC 8259</a>
 */
public interface JSONGenerator {

    /**
     * Generates String in JSON notation of the specified {@code JSONValue}.
     *
     * By default the output from
     * {@link #write(it.bewares.json.tree.JSONValue, java.io.Writer) write} is
     * returned, however that method depends on this one.
     *
     * @param json value for which a JSON text shall be generated
     * @return JSON text representing the {@code JSONValue}
     */
    default public String generate(JSONValue json) throws JSONFormatException {
        StringWriter writer = new StringWriter();

        try {
            this.write(json, writer);
        } catch (IOException ex) {
            throw new JSONFormatException(ex);
        }

        return writer.toString();
    }

    /**
     * Writes the String in JSON notation of the specified value to a respective
     * Writer.
     *
     * By default the output from
     * {@link #generate(it.bewares.json.tree.JSONValue) generate} is written,
     * however it is highly recommended to implement an own writing method.
     *
     * @param json value for which a JSON text shall be fed to the specified
     * Writer
     * @param writer Writer to be fed with a String containing valid JSON
     * @throws IOException when the write action fails
     */
    default public void write(JSONValue json, Writer writer) throws JSONFormatException, IOException {
        writer.write(generate(json));
    }

}
