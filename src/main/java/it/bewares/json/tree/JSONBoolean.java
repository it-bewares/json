/**
 *
 * Copyright 2017-2018 Markus Goellnitz.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.bewares.json.tree;

import java.util.Objects;
import lombok.Getter;
import lombok.Setter;
import it.bewares.json.JSONFactory;

/**
 * This class is a wrapper for a {@link java.lang.Boolean} and represents it in
 * JSON. A {@code JSONBoolean} contains only the represented {@code Boolean}.
 *
 * JSON refers to the JavaScript Object Notation Data Interchange Format in its
 * definition by the IETF in RFC 8259.
 *
 * @author Markus Goellnitz
 * @see <a href="https://tools.ietf.org/html/rfc8259">RFC 8259</a>
 */
public class JSONBoolean implements JSONPrimitive<JSONBoolean> {

    private static final long serialVersionUID = 6029840033745188834L;

    @Getter
    @Setter
    private Boolean value;

    /**
     * The {@code JSONBoolean} object corresponding to the Boolean
     * {@code Boolean.TRUE}.
     */
    public static final JSONBoolean TRUE = new JSONBoolean(true);

    /**
     * The {@code JSONBoolean} object corresponding to the Boolean
     * {@code Boolean.FALSE}.
     */
    public static final JSONBoolean FALSE = new JSONBoolean(false);

    /**
     * Allocates a {@code JSONBoolean} object wrapping the {@code value}
     * argument.
     *
     * @param value corresponding Boolean
     */
    public JSONBoolean(Boolean value) {
        this.value = value;
    }

    /**
     * Allocates a {@code JSONBoolean} object wrapping the {@code Boolean} which
     * is parsed by {@link java.lang.Boolean#valueOf(java.lang.String)}.
     *
     * @param value String to be converted to the corresponding Boolean
     */
    public JSONBoolean(String value) {
        this(Boolean.valueOf(value));
    }

    /**
     * Allocates a {@code JSONBoolean} by default wrapping the {@code false}.
     */
    public JSONBoolean() {
        this(false);
    }

    /**
     * Returns the value of this object as a primitive boolean.
     *
     * @return primitive {@code boolean} value of this object.
     */
    public boolean booleanValue() {
        return this.getValue();
    }

    /**
     * Compares the value of this object with another {@code JSONBoolean}.
     *
     * @param b JSONBoolean to be compared with
     * @return integer is negative for
     * {@code JSONBoolean.FALSE.compareTo(JSONBoolean.TRUE)}, zero if both have
     * the same value and positive for
     * {@code JSONBoolean.TRUE.compareTo(JSONBoolean.FALSE)}
     */
    @Override
    public int compareTo(JSONBoolean b) {
        return this.getValue().compareTo(b.getValue());
    }

    /**
     * Compares this object with another Object to check if both are
     * {@code JSONBoolean}s and if their wrapped {@code Boolean}s are equal.
     *
     * @param o Object to be checked against
     * @return a {@code boolean}, {@code true} if o is a {@code JSONBoolean} and
     * both wrapped values are equal, otherwise {@code false}
     */
    @Override
    public boolean equals(Object o) {
        if (o instanceof JSONBoolean) {
            return this.getValue().equals(((JSONBoolean) o).getValue());
        }
        return false;
    }

    /**
     * Calculates a hash code value for this {@code JSONBoolean} based on its
     * wrapped {@code Boolean} hash code.
     *
     * @return a hash code value for this {@code JSONBoolean}
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 47 * hash + Objects.hashCode(this.value);
        return hash;
    }

    /**
     * Returns a String representation of this {@code JSONBoolean}. It is the
     * wrapped {@code Boolean}s representation and therefor either
     * {@code "true"} or {@code "false"}.
     *
     * @return a String representation, {@code "true"} or {@code "false"}
     */
    @Override
    public String toString() {
        return this.getValue().toString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public JSONFactory.Type type() {
        return JSONFactory.Type.BOOLEAN;
    }

}
