/**
 *
 * Copyright 2017-2018 Markus Goellnitz.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.bewares.json.tree;

import it.bewares.json.JSONFactory;
import it.bewares.json.Jsonable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import javax.inject.Inject;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * This class is a wrapper for a {@link java.util.Map} and represents it in
 * JSON. A {@code JSONObject} contains only the represented {@code Map}.
 *
 * JSON refers to the JavaScript Object Notation Data Interchange Format in its
 * definition by the IETF in RFC 8259.
 *
 * @author Markus Goellnitz
 * @see <a href="https://tools.ietf.org/html/rfc8259">RFC 8259</a>
 * @param <V> the type of mapped values which has to be a {@code JSONStructure}
 */
@Slf4j
public class JSONObject<V extends JSONValue> implements JSONStructure, Map<String, V> {

    private static final long serialVersionUID = 6817795288708667215L;

    @Setter
    @Inject
    private JSONFactory factory = null;

    private final HashMap<String, V> underlyingMap;

    /**
     * Allocates a {@code JSONObject} by default wrapping an empty {@code Map}.
     */
    public JSONObject() {
        this.underlyingMap = new HashMap<>();
    }

    /**
     * Allocates a {@code JSONObject} wrapping the given {@code map} argument.
     *
     * @param map corresponding Map
     */
    public JSONObject(Map map) {
        this();

        this.putAll(map);
    }

    /**
     * Allocates a {@code JSONObject} with a {@code Map} filled from the given
     * {@code jsonable} argument. The Map is filled based on the Method defined
     * in the interface {@link it.bewares.json.Jsonable#json()}.
     *
     * @param jsonable Jsonable representing the corresponding Map
     */
    public JSONObject(Jsonable jsonable) {
        this(jsonable.json());
    }

    /**
     * Returns the number of key-value pairs in this map.
     *
     * @return the number of key-value pairs in this map
     */
    @Override
    public int size() {
        return this.underlyingMap.size();
    }

    /**
     * Determines if this map contains no key-value pairs.
     *
     * @return {@code true} if this map contains no key-value pairs, otherwise
     * {@code false}
     */
    @Override
    public boolean isEmpty() {
        return this.underlyingMap.isEmpty();
    }

    /**
     * Determines if this map contains the specified key.
     *
     * @param key key whose presence in this map shall be tested
     * @return {@code true} if this map contains the key, otherwise
     * {@code false}
     */
    @Override
    public boolean containsKey(Object key) {
        if (key instanceof CharSequence) {
            return this.underlyingMap.containsKey(key.toString());
        }
        return false;
    }

    /**
     * Determines if this map contains the specified value.
     *
     * @param value value whose presence in this map shall be tested
     * @return {@code true} if this map contains the value, otherwise
     * {@code false}
     */
    @Override
    public boolean containsValue(Object value) {
        return this.underlyingMap.containsValue(this.getFactory().createValue(value));
    }

    /**
     * Returns the value corresponding to the specified key, or {@code null} if
     * this map contains no corresponding value for the key.
     *
     * @param key the key whose associated value shall be returned
     * @return the value corresponding to the specified key, or {@code null}
     */
    @Override
    public V get(Object key) {
        if (key instanceof CharSequence) {
            return this.underlyingMap.get(key.toString());
        }
        return null;
    }

    /**
     * Associates the specified value with the specified key in this map. If the
     * map previously contained a mapping for the key, the old value is replaced
     * by the specified value.
     *
     * @param key key with which the value shall to be associated
     * @param value value to be associated with the key
     * @return the previous value associated with {@code key}
     */
    @Override
    public V put(String key, V value) {
        return this.underlyingMap.put(key, value);
    }

    /**
     * Associates the specified value with the specified key in this map. If the
     * map previously contained a mapping for the key, the old value is replaced
     * by the specified value.
     *
     * @param key key with which the value shall to be associated
     * @param value value to be associated with the key
     * @return the previous value associated with {@code key}
     */
    public V put(CharSequence key, Object value) {
        return this.put(key.toString(), this.getFactory().<V>createValue(value));
    }

    /**
     * Removes the key-value pair corresponding to the specified key from this
     * map.
     *
     * @param key key whose mapping is to be removed from the map
     * @return former value associated with {@code key}, or {@code null}
     */
    @Override
    public V remove(Object key) {
        if (key instanceof CharSequence) {
            return this.underlyingMap.remove(key.toString());
        }
        return null;
    }

    /**
     * Copies all of the key-value pairs from the specified map to this map.
     *
     * @param map mappings to be stored in this map
     */
    @Override
    public void putAll(Map map) {
        Map<String, V> n = new HashMap<>();
        for (Object key : map.keySet()) {
            n.put(Objects.toString(key), this.getFactory().<V>createValue(map.get(key)));
        }

        this.underlyingMap.putAll(n);
    }

    /**
     * Removes all of the key-value pairs from this map, such that it will be
     * empty.
     */
    @Override
    public void clear() {
        this.underlyingMap.clear();
    }

    /**
     * Returns a {@link Set} containing the keys of this map. The set is backed
     * by the map, so changes to the map are reflected in the set, and
     * vice-versa.
     *
     * @return a set containing the keys of this map
     */
    @Override
    public Set<String> keySet() {
        return this.underlyingMap.keySet();
    }

    /**
     * Returns a {@link Collection} containing the values of this map. The
     * collection is backed by the map, so changes to the map are reflected in
     * the collection, and vice-versa.
     *
     * @return a collection containing the values of this map
     */
    @Override
    public Collection<V> values() {
        return this.underlyingMap.values();
    }

    /**
     * Returns a {@link Set} containing the key-value pairs of this map. The set
     * is backed by the map, so changes to the map are reflected in the set, and
     * vice-versa.
     *
     * @return a set containing the key-value pairs of this map
     *
     */
    @Override
    public Set<Entry<String, V>> entrySet() {
        return this.underlyingMap.entrySet();
    }

    /**
     * Returns the {@code JSONFactory} that is currently in used for connection
     * with different modules.
     *
     * If there is none, one will be instanciated.
     *
     * @return the {@code JSONFactory} that is currently in use
     */
    public JSONFactory getFactory() {
        if (this.factory == null) {
            this.setFactory(new JSONFactory());
        }

        return this.factory;
    }

    /**
     * Compares this object with another Object to check if both are
     * {@code JSONObject}s and if their wrapped {@code Map}s are equal.
     *
     * @param b Object to be checked against
     * @return a {@code boolean}, {@code true} if o is a {@code JSONObject} and
     * both wrapped Maps are equal, otherwise {@code false}
     */
    @Override
    public boolean equals(Object b) {
        if (b instanceof JSONObject) {
            return this.underlyingMap.equals(((JSONObject) b).underlyingMap);
        }
        return false;
    }

    /**
     * Calculates a hash code value for this {@code JSONObject} based on its
     * wrapped {@code Map}s hash code.
     *
     * @return a hash code value for this {@code JSONObject}
     */
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 61 * hash + Objects.hashCode(this.underlyingMap);
        return hash;
    }

    /**
     * Returns the String representation of this {@code JSONObject}.
     *
     * @return a String representation
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("JSONObject{");

        int counter = this.keySet().size();
        for (String key : this.keySet()) {
            V value = this.get(key);

            sb.append(key);
            sb.append(": ");

            if (value == null) {
                sb.append("null");
            } else if (value.type().isStructure()) {
                sb.append(JSONFactory.Type.OBJECT.equals(value.type()) ? "{...}" : "[...]");
            } else if (JSONFactory.Type.STRING.equals(value.type())) {
                sb.append("\"...\"");
            } else {
                sb.append(Objects.toString(value));
            }
            if (--counter <= 0) {
                sb.append('}');
            } else {
                sb.append(", ");
            }
        }

        return sb.toString();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public JSONFactory.Type type() {
        return JSONFactory.Type.OBJECT;
    }

}
