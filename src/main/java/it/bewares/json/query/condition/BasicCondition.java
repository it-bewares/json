/**
 *
 * Copyright 2018 Markus Goellnitz.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.bewares.json.query.condition;

import it.bewares.json.query.exception.JSONQueryFormatException;
import it.bewares.json.query.JSONSelector;
import it.bewares.json.tree.JSONNumber;
import it.bewares.json.tree.JSONString;
import it.bewares.json.tree.JSONValue;
import java.util.Objects;
import java.util.regex.Pattern;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * A {@code BasicCondition} is a statement, either true or or false, for a given
 * {@code JSONValue}, its absolute path and respective key (or index).
 *
 * A {@code BasicCondition} only consists of two values and a comparator.
 *
 * JSON refers to the JavaScript Object Notation Data Interchange Format in its
 * definition by the IETF in RFC 8259.
 *
 * @author Markus Goellnitz
 * @see <a href="https://tools.ietf.org/html/rfc8259">RFC 8259</a>
 */
@Slf4j
public class BasicCondition extends Condition {

    /**
     * This enum lists all possible {@code Comparator}s for the
     * {@code BasicCondition}
     */
    public static enum Comparator {

        LESS,
        GREATER,
        LESS_OR_EQUAL,
        GREATER_OR_EQUAL,
        EQUAL,
        NOT_EQUAL,
        MATCH;
    }

    private static JSONValue resolveDefiniteSelector(String selector, JSONValue relative, JSONValue absolute, JSONValue key) {
        String definiteSelector = selector.substring(1, selector.length());
        JSONValue value;

        switch (selector.charAt(0)) {
            case '#':
                value = absolute;
                break;

            case '@':
                if (selector.startsWith("@key")) {
                    definiteSelector = selector.substring(4, selector.length());
                    value = (JSONString) key;
                } else if (selector.startsWith("@index")) {
                    definiteSelector = selector.substring(6, selector.length());
                    value = (JSONNumber) key;
                } else {
                    value = relative;
                }
                break;

            default:
                throw new JSONQueryFormatException(selector + " is not a valid selector");
        }

        return new JSONSelector(definiteSelector).find(value);
    }

    @Getter
    @Setter
    private Comparator comparator;

    private boolean aDynamic = true;
    @Getter
    private Object a;

    private boolean bDynamic = true;
    private boolean bPattern;
    @Getter
    private Object b;

    private Boolean staticValue;

    /**
     * Allocates a {@code BasicCondition} with the specified values a and b, and
     * the specified comparator.
     *
     * @param a the first value; a Selector String or {@code JSONValue}
     * @param comparator the comparator to compare a with b
     * @param b the second value; a Selector String, {@code JSONValue} or
     * {@code Pattern}
     */
    public BasicCondition(Object a, Comparator comparator, Object b) {
        this.setComparator(comparator);
        this.setA(a);
        this.setB(b);
    }

    /**
     * Sets the value a to the specified object.
     *
     * If it is a String it is treated as a definite Selector String with either
     * a preceding {@code {@literal @}} representing a relative Selector or
     * {@code #} representing a absolute Selector.
     *
     * If it is a {@code JSONValue} it is treated as itself.
     *
     * If it is non of the above it is an invalid value.
     *
     * @param a the first value for the comparison
     * @throws IllegalArgumentException if the specified value is not valid
     */
    public void setA(Object a) {
        if (a instanceof JSONValue) {
            LOG.debug("() {} is a json value", a.toString());
            this.aDynamic = false;
        } else if (a instanceof String) {
            LOG.debug("() {} is a string", (String) a);
            this.aDynamic = true;
        } else {
            throw new IllegalArgumentException();
        }

        this.a = a;

        this.checkForStatic();
    }

    /**
     * Sets the value b to the specified object.
     *
     * If it is a String it is treated as a definite Selector String with either
     * a preceding {@code {@literal @}} representing a relative Selector or
     * {@code #} representing a absolute Selector.
     *
     * If it is a {@code JSONValue} it is treated as itself.
     *
     * If it is a {@code Pattern} it shall only be used in combination with the
     * {@code Comparator.Match}.
     *
     * If it is non of the above it is an invalid value.
     *
     * @param b the second value for the comparison
     * @throws IllegalArgumentException if the specified value is not valid
     */
    public void setB(Object b) {
        if (b instanceof JSONValue) {
            LOG.debug("() {} is a json value", b.toString());
            this.bDynamic = false;
        } else if (b instanceof String) {
            LOG.debug("() {} is a string", (String) b);
            this.bDynamic = true;
        } else if (b instanceof Pattern) {
            LOG.debug("() {} is a pattern", b.toString());
            this.bPattern = true;
            this.bDynamic = false;
        } else {
            throw new IllegalArgumentException();
        }

        this.b = b;

        this.checkForStatic();
    }

    private void checkForStatic() {
        if (!this.aDynamic && !this.bDynamic) {
            this.staticValue = this.applies(null, null, null);
        } else {
            this.staticValue = null;
        }
    }

    /**
     * {@inheritDoc}
     *
     * For that both values a and b are being converted to their respective
     * {@code JSONValue} representation.
     *
     * A {@code JSONValue} has no more a need for a conversion.
     *
     * A definite Selector String is resolved using the relative/absolute
     * values.
     *
     * A {@code Pattern} for b is used in combination with the
     * {@code Comparator.Match} to check if a matches it.
     */
    @Override
    public boolean applies(JSONValue relative, JSONValue absolute, JSONValue key) {
        if (this.staticValue != null) {
            return this.staticValue;
        }

        JSONValue aVal;
        JSONValue bVal = null;
        Pattern bPattern = null;

        if (this.aDynamic) {
            aVal = BasicCondition.resolveDefiniteSelector((String) this.a, relative, absolute, key);
        } else {
            aVal = (JSONValue) a;
        }

        if (this.bPattern) {
            bPattern = (Pattern) b;
        } else if (this.bDynamic) {
            bVal = BasicCondition.resolveDefiniteSelector((String) this.b, relative, absolute, key);
        } else {
            bVal = (JSONValue) b;
        }

        switch (comparator) {
            case LESS:
                return ((JSONNumber) aVal).lessThan(((JSONNumber) bVal));
            case GREATER:
                return ((JSONNumber) aVal).greaterThan(((JSONNumber) bVal));
            case LESS_OR_EQUAL:
                return ((JSONNumber) aVal).lessThanOrEquals(((JSONNumber) bVal));
            case GREATER_OR_EQUAL:
                return ((JSONNumber) aVal).greaterThanOrEquals(((JSONNumber) bVal));
            case EQUAL:
                return aVal.equals(bVal);
            case NOT_EQUAL:
                return !aVal.equals(bVal);
            case MATCH:
                return bPattern.matcher(((JSONString) aVal).getValue()).find();
            default:
                throw new JSONQueryFormatException("\"" + comparator.name() + "\" is not a valid comparator");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(Object b) {
        if (!(b instanceof BasicCondition)) {
            return false;
        }

        if (!this.a.equals(((BasicCondition) b).a)) {
            return false;
        }

        if (!this.b.equals(((BasicCondition) b).b)) {
            return false;
        }

        if (!this.comparator.equals(((BasicCondition) b).comparator)) {
            return false;
        }

        return super.equals(b);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        int hash = super.hashCode() + 7;
        hash = 53 * hash + Objects.hashCode(this.comparator);
        hash = 53 * hash + Objects.hashCode(this.a);
        hash = 53 * hash + Objects.hashCode(this.b);
        return hash;
    }

}
