/**
 *
 * Copyright 2018 Markus Goellnitz.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.bewares.json.test;

import it.bewares.json.JSONFactory;
import it.bewares.json.io.exception.JSONFormatException;
import it.bewares.json.io.generator.JSONGenerator;
import it.bewares.json.io.generator.SimpleJSONGenerator;
import it.bewares.json.io.parser.JSONParser;
import it.bewares.json.io.parser.JSONStreamParser;
import it.bewares.json.query.JSONQuery;
import it.bewares.json.tree.JSONValue;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * This test class is to test the JSONFactory.
 *
 * @author Markus Goellnitz
 */
public class FactoryTest {

    public static class MockingParser implements JSONParser {

        @Override
        public JSONValue parse(String input) throws JSONFormatException {
            return new MockingValue(42);
        }

    }

    public static class MockingGenerator implements JSONGenerator {

        @Override
        public String generate(JSONValue json) throws JSONFormatException {
            return "It works!";
        }

    }

    public static class MockingParser2 extends MockingParser {

        public MockingParser2(int i) {
        }

    }

    public static class MockingGenerator2 extends MockingGenerator {

        public MockingGenerator2(int i) {
        }

    }

    public static class MockingValue implements JSONValue {

        private static final long serialVersionUID = -1401916087594681361L;

        private int i;

        public MockingValue(int i) {
            this.i = i;
        }

        @Override
        public JSONFactory.Type type() {
            return null;
        }

        @Override
        public Object clone() throws CloneNotSupportedException {
            return null;
        }

        @Override
        public boolean equals(Object o) {
            return o instanceof MockingValue && ((MockingValue) o).i == this.i;
        }

        @Override
        public int hashCode() {
            return 0;
        }

    }

    public static class MockingQuery implements JSONQuery {

        @Override
        public Collection<JSONValue> resolve(JSONValue input) {
            return new HashSet<>(Arrays.asList(new MockingValue(1), new MockingValue(2)));
        }

    }

    private JSONFactory factory = new JSONFactory(MockingParser.class, MockingGenerator.class);

    @Test
    private void instanciation() {
        JSONFactory factory = new JSONFactory();

        Assert.assertTrue(factory.getParser() == null);
        Assert.assertTrue(factory.getGenerator() == null);

        factory = new JSONFactory(MockingParser2.class, MockingGenerator2.class);

        Assert.assertTrue(factory.getParser() == null);
        Assert.assertTrue(factory.getGenerator() == null);

        factory = new JSONFactory(JSONStreamParser.class, SimpleJSONGenerator.class);

        Assert.assertTrue(factory.getParser() instanceof JSONStreamParser);
        Assert.assertTrue(factory.getGenerator() instanceof SimpleJSONGenerator);

        factory.setParser(new MockingParser());
        factory.setGenerator(new MockingGenerator());

        Assert.assertTrue(factory.getParser() instanceof MockingParser);
        Assert.assertTrue(factory.getGenerator() instanceof MockingGenerator);
    }

    @Test
    private void io() {
        Assert.assertEquals(factory.generate(null), "It works!");
        Assert.assertEquals(factory.parse("Answer to the Ultimate Question of Life, The Universe, and Everything"), new MockingValue(42));
    }

    @Test
    private void queries() {
        JSONQuery query = new MockingQuery();
        Assert.assertEquals(factory.find(query, null), new MockingValue(1));
        Assert.assertEquals(factory.resolve(query, null), new HashSet(Arrays.asList(new MockingValue(1), new MockingValue(2))));
    }

    @Test
    private void valueFinding() {
        //factory.valueOf(this);
    }

    @Test
    private void typeEnum() {
        //factory.valueOf(this);
    }

}
