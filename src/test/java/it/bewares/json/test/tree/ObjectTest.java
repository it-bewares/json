/**
 *
 * Copyright 2018 Markus Goellnitz.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.bewares.json.test.tree;

import it.bewares.json.JSONFactory;
import it.bewares.json.Jsonable;
import it.bewares.json.tree.JSONNumber;
import it.bewares.json.tree.JSONObject;
import it.bewares.json.tree.JSONString;
import it.bewares.json.tree.JSONValue;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import org.testng.Assert;
import org.testng.annotations.Test;

/**
 * This test class is to test the JSONObject.
 *
 * @author Markus Goellnitz
 */
public class ObjectTest {

    private JSONFactory JSON = new JSONFactory();

    Map aVal = new HashMap<String, Object>() {
        {
            put("a", new Object[]{null, true});
            put("b", false);
            put("c", 2);
            put("d", new HashMap());
            put("e", "hi");
            put("f", null);
        }
    };
    JSONObject a = new JSONObject(aVal);

    @Test
    public void instanciation() {
        Assert.assertEquals(new JSONObject().size(), 0);

        Assert.assertEquals(new JSONObject(new Jsonable() {
            @Override
            public JSONObject json() {
                return a;
            }
        }), a);
        Assert.assertEquals(a.entrySet(), new JSONObject(aVal).entrySet());
    }

    @Test
    public void mapSpecific() {
        JSONObject<JSONValue> b = new JSONObject();

        b.putAll(aVal);
        Assert.assertTrue(b.containsKey("b"));
        Assert.assertFalse(b.containsKey("g"));
        Assert.assertFalse(b.containsKey(JSON));

        Assert.assertTrue(b.containsValue(false));
        Assert.assertFalse(b.containsValue(true));

        Assert.assertEquals(b.get("c"), new JSONNumber(2));
        Assert.assertEquals(b.get(JSON), null);

        Assert.assertFalse(b.isEmpty());

        Assert.assertEquals(b.size(), 6);

        Assert.assertEquals(b.remove("e"), new JSONString("hi"));

        Assert.assertEquals(b.size(), 5);

        Assert.assertEquals(b.remove(35), null);

        Assert.assertEquals(b.size(), 5);

        Assert.assertEquals(b.keySet(), new HashSet(Arrays.asList("a", "b", "c", "d", "f")));

        for (Map.Entry<String, JSONValue> entry : b.entrySet()) {
            Assert.assertEquals(entry.getValue(), JSON.createValue(aVal.get(entry.getKey())));

            Assert.assertTrue(b.keySet().contains(entry.getKey()));
        }

        b.clear();
        Assert.assertTrue(b.isEmpty());

        Assert.assertEquals(b.size(), 0);

        b.put("hey", "there");
        b.put(new JSONString("believe"), "me");

        Assert.assertEquals(b.size(), 2);

        for (JSONValue value : b.values()) {
            Assert.assertTrue(new ArrayList(Arrays.asList(new JSONString("there"), new JSONString("me"))).contains(value));
        }
    }

    @Test
    public void valueSpecific() throws CloneNotSupportedException {
        Assert.assertEquals(a.type(), JSONFactory.Type.OBJECT);

        Assert.assertEquals(a.clone(), a);
    }

    @Test
    public void objectMethods() {
        Assert.assertEquals(a.hashCode(), 8708);

        Assert.assertTrue(a.equals(new JSONObject(aVal)));

        Assert.assertEquals(a.toString(), "JSONObject{a: [...], b: false, c: 2, d: {...}, e: \"...\", f: null}");
    }

}
