/**
 *
 * Copyright 2018 Markus Goellnitz.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.bewares.json.test.io.generator;

import it.bewares.json.io.exception.JSONRecursionException;
import java.util.Arrays;
import java.util.HashMap;
import it.bewares.json.tree.JSONArray;
import it.bewares.json.tree.JSONBoolean;
import it.bewares.json.tree.JSONNumber;
import it.bewares.json.tree.JSONObject;
import it.bewares.json.tree.JSONString;
import it.bewares.json.io.generator.JSONGenerator;
import it.bewares.json.io.generator.SimpleJSONGenerator;
import it.bewares.json.tree.JSONValue;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.Method;
import org.testng.Assert;
import org.testng.Assert.ThrowingRunnable;
import org.testng.ITest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Factory;
import org.testng.annotations.Test;

/**
 * This test class is to test the JSONGenerator.
 *
 * @author Markus Goellnitz
 */
public class GenerationTest implements ITest {

    JSONGenerator generator;
    String itemSeparator;

    @Factory(dataProvider = "generators")
    public GenerationTest(JSONGenerator generator, String itemSeparator) {
        this.generator = generator;
        this.itemSeparator = itemSeparator;
    }

    @DataProvider
    public static Object[][] generators() {
        return new Object[][]{
            {new SimpleJSONGenerator(), ","}
        };
    }

    @Test
    public void objectGeneration() {
        Assert.assertEquals(generator.generate(new JSONObject()), "{}");
        Assert.assertEquals(generator.generate(new JSONObject(new HashMap() {
            {
                put("this", "failed?");
            }
        })), "{\"this\":\"failed?\"}");
        Assert.assertEquals(generator.generate(new JSONObject(new HashMap() {
            {
                put("this", "failed?");
                put("so", "true");
                put("except it!", "now, or hey wowo, dat is it! WHUT???? NOO!!!!! yes. :C but?");
            }
        })), "{\"this\":\"failed?\""
                + itemSeparator + "\"so\":\"true\""
                + itemSeparator + "\"except it!\":\"now, or hey wowo, dat is it! WHUT???? NOO!!!!! yes. :C but?\"}");
    }

    @Test
    public void arrayGeneration() {
        Assert.assertEquals(generator.generate(new JSONArray()), "[]");
        Assert.assertEquals(generator.generate(new JSONArray(JSONBoolean.TRUE)), "[true]");
        Assert.assertEquals(generator.generate(new JSONArray(new JSONString("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), JSONBoolean.FALSE, JSONBoolean.TRUE)), "[\"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ\""
                + itemSeparator + "false"
                + itemSeparator + "true]");
    }

    @Test
    public void numberGeneration() {
        Assert.assertEquals(generator.generate(new JSONNumber("0")), "0");
        Assert.assertEquals(generator.generate(new JSONNumber("1")), "1");
        Assert.assertEquals(generator.generate(new JSONNumber("-3")), "-3");
        Assert.assertEquals(generator.generate(new JSONNumber("-5.0e-3")), "-0.0050");
        Assert.assertEquals(generator.generate(new JSONNumber("4e2")), "4E+2");
    }

    @Test
    public void stringGeneration() {
        Assert.assertEquals(generator.generate(new JSONString("")), "\"\"");
        Assert.assertEquals(generator.generate(new JSONString("\"")), "\"\\\"\"");
        Assert.assertEquals(generator.generate(new JSONString("hey, whut's up?")), "\"hey, whut's up?\"");
        Assert.assertEquals(generator.generate(new JSONString("line 1\nline2")), "\"line 1\\nline2\"");
        Assert.assertEquals(generator.generate(new JSONString("line 1\rline2")), "\"line 1\\rline2\"");
        Assert.assertEquals(generator.generate(new JSONString("a\tb")), "\"a\\tb\"");
        Assert.assertEquals(generator.generate(new JSONString("\n\r\t\b\f\f\b\t\r\n\u001a\u000b")), "\"\\n\\r\\t\\b\\f\\f\\b\\t\\r\\n\\u001a\\u000b\"");
    }

    //@Test
    public void invalidGenerationRecursion() {
        Assert.assertThrows(JSONRecursionException.class, new ThrowingRunnable() {

            @Override
            public void run() throws Throwable {
                JSONObject a = new JSONObject();
                JSONArray b = new JSONArray();
                b.add((JSONValue) a);
                a.put("", b);

                generator.generate(a);

                throw new Exception("did generate");
            }
        });
    }

    @Test
    public void arbitraryGeneration() {
        Assert.assertEquals(generator.generate(new JSONObject(new HashMap() {
            {
                put("this", Arrays.asList());
            }
        })), "{\"this\":[]}");
        Assert.assertEquals(generator.generate(new JSONObject(new HashMap() {
            {
                put("this", Arrays.asList("failed!", "or", "did not"));
            }
        })), "{\"this\":[\"failed!\""
                + itemSeparator + "\"or\""
                + itemSeparator + "\"did not\"]}");
        Assert.assertEquals(generator.generate(new JSONObject(new HashMap() {
            {
                put("this", Arrays.asList("failed?", new HashMap() {
                    {
                        put("was", "successful");
                    }
                }));
            }
        })), "{\"this\":[\"failed?\""
                + itemSeparator + "{\"was\":\"successful\"}]}");
        Assert.assertEquals(generator.generate(new JSONObject(new HashMap() {
            {
                put("this", Arrays.asList("failed?", new HashMap() {
                    {
                        put("with", null);
                    }
                }));
            }
        })), "{\"this\":[\"failed?\""
                + itemSeparator + "{\"with\":null}]}");
        Assert.assertEquals(generator.generate(new JSONArray(new JSONArray(), new JSONObject(), JSONBoolean.TRUE, new JSONNumber(1), new JSONNumber(2))), "[[]"
                + itemSeparator + "{}"
                + itemSeparator + "true"
                + itemSeparator + "1"
                + itemSeparator + "2]");
        Assert.assertEquals(generator.generate(new JSONArray(new JSONArray(new JSONArray(new JSONArray(), new JSONObject())), new JSONObject(), JSONBoolean.TRUE, new JSONNumber(1), null, JSONBoolean.FALSE, new JSONNumber(2))), "[[[]" + itemSeparator + "{}]"
                + itemSeparator + "{}"
                + itemSeparator + "true"
                + itemSeparator + "1"
                + itemSeparator + "null"
                + itemSeparator + "false"
                + itemSeparator + "2]");
    }

    @Test
    public void alternativeInput() throws IOException {
        Writer writer = new StringWriter();
        generator.write(new JSONArray(new JSONArray(new JSONArray(new JSONArray(), new JSONObject())), new JSONObject(), JSONBoolean.TRUE, new JSONNumber(1), null, JSONBoolean.FALSE, new JSONNumber(2)), writer);
        Assert.assertEquals(writer.toString(), "[[[]" + itemSeparator + "{}]"
                + itemSeparator
                + "{}"
                + itemSeparator + "true"
                + itemSeparator + "1"
                + itemSeparator + "null"
                + itemSeparator + "false"
                + itemSeparator + "2]");
    }

    private String testName = "";

    @BeforeMethod(alwaysRun = true)
    public void setTestName(Method method) {
        this.testName = String.format("%s(%s)", method.getName(), this.generator.getClass().getSimpleName());
    }

    @Override
    public String getTestName() {
        return testName;
    }

}
