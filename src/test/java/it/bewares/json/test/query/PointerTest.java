/**
 *
 * Copyright 2018 Markus Goellnitz.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 */
package it.bewares.json.test.query;

import it.bewares.json.io.parser.JSONStreamParser;
import it.bewares.json.query.exception.JSONQueryFormatException;
import it.bewares.json.query.exception.JSONQueryResolveException;
import it.bewares.json.query.JSONPointer;
import it.bewares.json.tree.JSONNumber;
import it.bewares.json.tree.JSONValue;
import java.util.ArrayList;
import java.util.Arrays;
import org.testng.Assert;
import org.testng.Assert.ThrowingRunnable;
import org.testng.annotations.Test;

/**
 * This test class is to test the JSONPointer to the JSON Pointer standard.
 *
 * JSON Pointer refers to the JavaScript Object Notation Pointer in its
 * definition by the IETF in RFC 6901.
 *
 * JSON refers to the JavaScript Object Notation Data Interchange Format in its
 * definition by the IETF in RFC 8259.
 *
 * @author Markus Goellnitz
 * @see <a href="https://tools.ietf.org/html/rfc6901">RFC 6901</a>
 * @see <a href="https://tools.ietf.org/html/rfc8259">RFC 8259</a>
 */
public class PointerTest {

    JSONValue testVal = new JSONStreamParser().parse("{\"planets\":[{\"name\":\"Earth\",\"technologicallyAdvancedSpecies\":\"homo sapiens\",\"hasBuiltSpaceships\":true,\"moons\":1},{\"name\":\"Kerbin\",\"technologicallyAdvancedSpecies\":\"Kerbal\",\"hasBuiltSpaceships\":true,\"moons\":2}],\"stars\":[]}");

    @Test
    public void invalidFormats() {
        for (String pointer : Arrays.asList("a")) {
            Assert.assertThrows(JSONQueryFormatException.class, new ThrowingRunnable() {

                @Override
                public void run() throws Throwable {
                    new JSONPointer(pointer);
                    throw new Exception("did accept invalid pointer: " + pointer);
                }
            });
        }
    }

    @Test
    public void invalidQueries() {
        for (String pointer : Arrays.asList("/palnets/0/moons",
                "/planets/31",
                "/planets/0/moons/1")) {
            Assert.assertThrows(JSONQueryResolveException.class, new Assert.ThrowingRunnable() {

                @Override
                public void run() throws Throwable {
                    JSONValue result = new JSONPointer(pointer).find(testVal);
                    throw new Exception("did accept pointer: " + pointer + " and found " + result);
                }
            });
        }
    }

    @Test
    public void validQueries() {
        Assert.assertEquals(new JSONPointer("").find(testVal), testVal);
        Assert.assertEquals(new JSONPointer("/planets/0/moons").find(testVal), new JSONNumber(1));
        Assert.assertEquals(new JSONPointer("/planets/0/moons").resolve(testVal), new ArrayList<>(Arrays.asList(new JSONNumber(1))));
    }

    @Test
    public void pointerSpecific() {
        Assert.assertEquals(new JSONPointer().find(testVal), new JSONPointer("").find(testVal));
        Assert.assertEquals(new JSONPointer(new String[]{"planets", "0"}).getPointer(), "/planets/0");
        Assert.assertEquals(new JSONPointer("/planets/0").getPointer(), "/planets/0");
        Assert.assertEquals(new JSONPointer("/planets/0").getPath(), new String[]{"planets", "0"});
        Assert.assertEquals(new JSONPointer("/planets/0/moons").getParentPointer().getPath(), new JSONPointer("/planets/0").getPath());
    }
}
